<?php

class User {
     private $name;
     private $username;
     private $email;
     private $password;

     public function __construct($name) {
         $this->name = $name;
     }

     public function getName() {
         return $this->name;
     }

     public function setName($name) {
         $this->name = $name;
     }
}

?>