<?php

class AppController {
    private $request = null;

    public function __construct() {
        $this->request = strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function isGet() {
        return $this->request === 'get';
    }

    public function isPost() {
        return $this->request === 'post';
    }

    public function render(string $filename, $variables = []) {
        $path = $filename ? dirname(__DIR__).'/views//'.get_class($this).'//'.$filename.'.php' : ''; // '\\' - masks '\'

        $output = "There isn't such file to render.";

        if(file_exists($path)) {
            extract($variables);
            
            ob_start();

            include $path;
            $output = ob_get_clean();
        }

        print $output;
    }
}

?>