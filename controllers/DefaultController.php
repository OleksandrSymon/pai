<?php

require_once("AppController.php");

class DefaultController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $name = 'Alex';
        $this->render('index', ['name' => $name]);
    }

    public function login() 
    {
        $this->render('login');
    }
}

?>