<?php

require_once('controllers/DefaultController.php');
require_once('controllers/UploadController.php');

class Routing
{
    public $routes = [];

    public function __construct()
    {
        $this->routes = [
            'index' => [
                'action' => 'index',
                'controller' => 'DefaultController'
            ],
            'login' => [
                'action' => 'login',
                'controller' => 'DefaultController'
            ],
            'upload' => [
                'action' => 'upload',
                'controller' => 'UploadController'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'index';

        if($this->routes[$page]) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}

?>